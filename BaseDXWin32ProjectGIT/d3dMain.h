#pragma once
#include "d3dApp.h"
#include "DirectInput.h"
#include "XBOXController.h"


class GameFramework : public D3DApp
{
public:
	GameFramework(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP);
	~GameFramework();

	bool checkDeviceCaps();

	bool isPaused();
	void SetPause(bool b);
	void TogglePause();
	
	void onLostDevice();
	void onResetDevice();
	void updateScene(float dt);
	void drawScene();

	POINT GetScreenSize();

private:

	bool m_paused;

	void EventXBOXController();
};